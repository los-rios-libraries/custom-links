<?php
/*
// show errors for debugging
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
*/
$url = $_GET['url'];

$domain = 'http://';
$proxyPr = '0-';
$proxy = '.lasiii.losrios.edu';
// check for proxy
if (strpos($url, $domain . $proxyPr) === false) {
    if (strpos($url, 'jstor') > -1) {
        $str = 'www.jstor.org';
    }
    elseif (strpos($url, 'sciencedirect') > -1) {
        $str = 'www.sciencedirect.com';
    }
    elseif (strpos($url, 'hdl.handle') > -1) {
        $str = 'hdl.handle.net';        
    }
        
    elseif (strpos($url, 'oxfordart') > -1) {
        $str = 'www.oxfordartonline.com';
    }
    
    elseif (strpos($url, 'fod.infobase') > -1) {
        $str = 'fod.infobase.com';
        $url = $url . '&wID=' . $_GET['wID'];
    }
    elseif(strpos($url, 'intelecom') > -1) { // need better solution for ssl
        $url = str_replace('http://searchcenter.intelecomonline.net', 'https://0-searchcenter-intelecomonline-net.lasiii.losrios.edu', $url);
        header('Location: ' . $url);
        exit;
    }
    $url = str_replace($str, $proxyPr . $str . $proxy, $url);
}
else {
//   echo 'proxy is there';
    if (strpos($url, 'fod.infobase') > -1) {
        $url = $url . '&wID=' . $_GET['wID'];
    }
    elseif (strpos($url, 'intelecom') > -1) {
        $url = str_replace('http://0-searchcenter.intelecomonline.net', 'https://0-searchcenter-intelecomonline-net', $url);
    }
    
}
/*
if ($str === 'digital.films.com') {
  echo $url;
  exit;

}
*/

header('Location: ' . $url);
 
?>